var express = require('express');
var router = express.Router();

var tasks = require('../src/providers/Tasks');

router.get('/list', function(req, res, next) {
	res.json(tasks.list());
});

router.get('/:id', function(req, res, next) {
	res.json(tasks.get(req.param('id')));
});

router.post('/', function(req, res, next) {
	res.json(tasks.post(req.body));
});

router.put('/', function(req, res, next) {
	res.send(tasks.put(req.body));
});

router.delete('/:id', function(req, res, next) {
	res.send(tasks.delete(req.param('id')));
});

module.exports = router;
