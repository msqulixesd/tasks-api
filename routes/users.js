var express = require('express');
var router = express.Router();

var users = require('../src/providers/Users');
var tasks = require('../src/providers/Tasks');

router.get('/list', function(req, res, next) {
	res.json(users.list());
});

router.get('/:id', function(req, res, next) {
	res.json(users.get(req.param('id')));
});

router.get('/:id/tasks', function(req, res, next) {
	res.json(tasks.getByUserId(req.param('id')));
});

router.post('/', function(req, res, next) {
	res.json(users.post(req.body));
});

router.put('/', function(req, res, next) {
	res.send(users.put(req.body));
});

router.delete('/:id', function(req, res, next) {
	res.send(users.delete(req.param('id')));
});

module.exports = router;
