var express = require('express');
var router = express.Router();

var projects = require('../src/providers/Projects');
var tasks = require('../src/providers/Tasks');

router.get('/list', function(req, res, next) {
	res.json(projects.list());
});

router.get('/:id', function(req, res, next) {
	res.json(projects.get(req.param('id')));
});

router.get('/:id/tasks', function(req, res, next) {
	res.json(tasks.getByProject(req.param('id')));
});

router.post('/', function(req, res, next) {
	res.json(projects.post(req.body));
});

router.put('/', function(req, res, next) {
	res.send(projects.put(req.body));
});

router.delete('/:id', function(req, res, next) {
	const id = req.param('id');
	projects.delete(id);
	tasks.removeByProject(id);
	res.sendStatus(200);
});

module.exports = router;
