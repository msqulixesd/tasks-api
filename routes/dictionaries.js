var express = require('express');
var router = express.Router();

var taskStatuses = require('../src/data/taskStatuses.json');
var positions = require('../src/data/positions.json');

	router.get('/taskStatuses', function(req, res, next) {
	res.send(taskStatuses);
});

router.get('/positions', function(req, res, next) {
	res.send(positions);
});

module.exports = router;
