var omitBy = require('lodash').omitBy;
var filter = require('lodash').filter;
var indexOf = require('lodash').indexOf;
var initialData = require('../data/tasks.json');
var AbstractProvider = require('./AbstractProvider');

class TasksProvider extends AbstractProvider {

	removeByProject(projectId) {
		this._data = omitBy(this._data, (task) => task.projectId == projectId);
	}

	getByProject(projectId) {
		return filter(this._data, (task) => task.projectId == projectId);
	}

	getByUserId(userId) {
		return filter(this._data, (task) => indexOf(task.userIds, Number(userId)) >= 0 );
	}

}


module.exports = new TasksProvider(initialData);