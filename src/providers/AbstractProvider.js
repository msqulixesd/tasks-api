var find = require('lodash').find;
var assign = require('lodash').assign;
var without = require('lodash').without;
var size = require('lodash').size;
var toArray = require('lodash').toArray;

class AbstractProvider {

	constructor(initialData) {
		this._data = initialData;
		this._incrementId = size(initialData);
	}

	list() {
		return toArray(this._data);
	}

	get(id) {
		const item = this.find(id);
		if (item) {
			return item;
		} else {
			this.throwNotFound();
		}
	}

	post(item) {
		item.id = this.getIncrementId();
		this._data.push(item);
		return item;
	}

	put(item) {
		const foundItem = this.find(item.id);
		if (foundItem) {
			return assign(foundItem, item);
		} else {
			this.throwNotFound();
		}
	}

	delete(id) {
		const item = this.find(id);

		if (item) {
			this._data = without(this._data, item);
		} else {
			this.throwNotFound();
		}
	}

	getIncrementId() {
		this._incrementId++;
		return this._incrementId;
	}

	find(id) {
		return find(this._data, { id: Number(id) });
	}

	throwNotFound() {
		throw Error('Item not found');
	}
}

module.exports = AbstractProvider;